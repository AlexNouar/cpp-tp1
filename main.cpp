//Fichier.main 
#include <iostream>
#include "TP.h"

using namespace std;

int main(int argc, char *argv[])
{
    tp blc(0);
    long i;
    i = 4;
    blc.fonc(i);
    cout << " adresse de i : " << &i << endl;
    cout << " dans main(), valeur de i avant fonc() " << i << endl;
    cout << " dans main(), valeur de i apres fonc() " << i << endl;
    return 0;
}
