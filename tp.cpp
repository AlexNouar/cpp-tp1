//
// Created by arsen on 24/09/2019.
//


#include "tp.h"
#include <iostream>
#include <stdlib.h>

using namespace std;
tp::tp(long x1)//Encapsulation des données en vue de sécuriser les valeurs, il est nécessaire de faire [nomclasse]::[nomclasse] pour faire l'encapsulation
{
    j1 = x1;
}

void tp::fonc(long j1)//Afin de faire une fonction, faire [nomclasse]::[nomfonction]
{
    cout << "adresse de j :" << &j1 <<endl;
    cout << "dans fonc(), valeur de j avant incrément :" << j1 << endl;
    j1++;
    cout << "dans fonc(), valeur de j apres incrément :" << j1 << endl;
    return;
}
